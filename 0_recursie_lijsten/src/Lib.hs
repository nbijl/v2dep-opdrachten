module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- | Schrijf een functie die de som van een lijst getallen berekent, net als de `product` functie uit de les.
-- 
-- Ex1 heeft als input een Int en als output ook een Int.
-- Als de invoer gelijk is aan een legen lijst ([]), dan returnt hij 0.
-- Als de invoer gelijk is aan een gevulde lijst dan pakt hij het eerste item uit de lijst en sommeert dat met zichzelf als functie met als invoer de rest van de lijst.
-- Daarna pakt hij het 2e element als head en roept zichzelf weer aan met de rest van de lijst.
-- Tot dat de lijst leeg is en dan sommeert die de hele lijst + 0 en returnt de som van de lijst.
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 list = (head list) + ex1 (tail list)

-- | Schrijf een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- 
-- Ex2 heeft als input een lijst van Int's en als output ook een lijst van Int's. 
-- Als de invoer gelijk is aan een legen lijst ([]), dan returnt hij een lege lijst ([]).
-- Als de invoer gelijk is aan een gevulde lijst dan pakt hij het eerste item uit de lijst en voegt daar 1 bij toe en stopt dat getal in de lijst van de functie die zichzelf aanroept met de overige lijst.
-- Zo herhaalt het zichzelf totdat de lijst leeg is en de functie een lege lijst returnt waar dan alle items uit de oude lijst worden toegevoegd waar 1 bij opgeteld is.
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 list = (head list + 1) : ex2 (tail list)

-- | Schrijf een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
--
-- Ex3 heeft als input een lijst van Int's en als output ook een lijst van Int's
-- Als de invoer gelijk is aan een legen lijst ([]), dan returnt hij een lege lijst ([])
-- Als de invoer gelijk is aan een gevulde lijst, dan pakt hij het eerste item uit de lijst en vermenigvuldigt dit met (-1) (de haakjes zijn nodig anders pakt hij de - niet goed)
-- Dan voegt hij het met (-1) vermenigvuldigde item toe aan de lijst van zichzelf als functie met als input de rest van de lijst en zo door tot de oude lijst leeg is
-- Als de lijst leeg is returnt hij een lege lijst en voegt daar alle met (-1) vermenigvuldigde items aan toe en returnt deze nieuwe lijst
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 list = (head list * (-1)) : ex3 (tail list)

-- | Schrijf een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] 
-- combineert tot [1,2,3,4,5,6]. Maak hierbij geen gebruik van de standaard-functies, 
-- maar los het probleem zelf met (expliciete) recursie op. Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
--
-- Ex4 heeft als input twee maal een lijst van Int's en als output 1 lijst van Int's
-- Als de eerste input gelijk is aan een lege lijst ([]) en de 2e input gelijk is aan een lijst dan returnt hij de 2e input
-- Als de eerste input gelijk is aan een gevulde lijst en de 2e input ook dan pakt die het eerste item uit de eerste input en voegt dat toe aan de lijst van de functie van zichzelf met als input de rest van de eerste input en de 2e input
-- Dit gaat door totdat de eerste input gelijk is aan een lege lijst, dan return hij de 2e input en voegt daar de items van de eerste input aan toe

ex4 :: [Int] -> [Int] -> [Int]
ex4 [] list2 = list2
ex4 list1 list2 = (head list1) : (ex4 (tail list1) list2)

-- | Schrijf een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- 
-- Ex5 heeft als input twee maal een lijst van Int's en als output 1 lijst van Int's
-- Als de 1e en de 2e input beide een lege lijst zijn dan return hij een lege lijst ([])
-- Als beide inputs een gevulde lijst zijn dan pakt hij het eerste item van beide lijsten en sommeert deze, dan wordt dat getal toegevoegd aan de lijst van zichzelf als functie met als input de rest van beide lijsten
-- Als deze lijsten beide leeg zijn dan returnt hij een lege lijst en worden de gesommeerde getallen hieraan toegevoegd
ex5 :: [Int] -> [Int] -> [Int]
ex5 [] [] = []
ex5 list1 list2 = ((head list1) + (head list2)) : (ex5 (tail list1) (tail list2))

-- | Schrijf een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- 
-- Ex6 heeft als input twee maal een lijst van Int's en als output 1 lijst van Int's
-- Als de 1e en de 2e input beide een lege lijst zijn dan return hij een lege lijst ([])
-- Als beide inputs een gevulde lijst zijn dan pakt hij het eerste item van beide lijsten en vermenigvuldigt deze met elkaar, dan wordt dat getal toegevoegd aan de lijst van zichzelf als functie met als input de rest van beide lijsten
-- Als deze lijsten beide leeg zijn dan returnt hij een lege lijst en worden de gesommeerde getallen hieraan toegevoegd
ex6 :: [Int] -> [Int] -> [Int]
ex6 [] [] = []
ex6 list1 list2 = ((head list1) * (head list2)) : (ex6 (tail list1) (tail list2))

<<<<<<< HEAD
-- | Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig prodct uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
-- 
-- Ex7 heeft als input twee maal een lijst van Int's en als output 1 lijst van Int's
-- Als de 1e en de 2e input beide een lege lijst zijn dan return hij het getal 0
-- Als beide inputs een gevulde lijst zijn dan pakt hij het eerste item van beide lijsten en vermenigvuldigt deze met elkaar, dan wordt dat getal gesommeert aan zichzelf als functie met als input de rest van beide lijsten
-- Als deze lijsten beide leeg zijn dan returnt hij het getal 0 en worden de vermenigvuldigde getallen hieraan toegevoegd
=======
-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig product uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
>>>>>>> 15f6e502c952be37312eddb3823ebe24ca7bc074
ex7 :: [Int] -> [Int] -> Int
ex7 [] [] = 0
ex7 list1 list2 = ((head list1) * (head list2)) + (ex7 (tail list1) (tail list2))


-- | ...
-- Copyright:
--  Niels Bijl, 3-9-2020
-- ...
