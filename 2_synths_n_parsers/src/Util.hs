{-|
    Module      : Types
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat enkele algemene polymorfe functies.
-}

module Util (zipWithL, zipWithR, comb, fst3, uncurry3) where

import Control.Applicative (liftA2)

-- | Version of ZipWith guaranteed to produce a list of the same length as the second input.
zipWithR :: (a -> b -> b) -> [a] -> [b] -> [b]
zipWithR _ _      []     = []
zipWithR _ []     bs     = bs
zipWithR f (a:as) (b:bs) = f a b : zipWithR f as bs

-- TODO Maak een `zipWithL` die als mirror-versie van `zipWithR` fungeert door de lengte gelijk te houden aan die van de eerste input in plaats van de tweede.
{- | Hij krijgt een functie mee en 2 lijsten
Eerst kijken we wat we doen als het niet uit maakt wat de functie input is en we een lege lijst mee krijgen en nog een input die niet uitmaakt dan returnen we een legen lijst
Dan kijken we als het niet uit maakt wat de functie input is, een lijst en een lege lijst. Dan returnen we de lijst
Dan kijken we als we een geldige functie meekrijgen een volle lijst en nog een vollelijst. Dan returnen we beide eerste elementen van de twee lijsten met daarop de functie uitgevoerd. Dat stoppen we in de lijst van het recursief aanroepen van zichzelf met beide tails van de twee lijsten meegeven. Deze recursie gaat door tot zipWithL een lege lijst returnt en dan worden alle waarders daar aan toegevoegd en die lijst gereturnd 
-}
zipWithL :: (a -> b -> a) -> [a] -> [b] -> [a]
zipWithL _ []      _     = []
zipWithL _ as     []     = as
zipWithL f (a:as) (b:bs) = f a b : zipWithL f as bs

-- | Use a given binary operator to combine the results of applying two separate functions to the same value. Alias of liftA2 specialised for the function applicative.
comb :: (b -> b -> b) -> (a -> b) -> (a -> b) -> a -> b
comb = liftA2

-- TODO Maak een functie `fst3` die het eerste element van een 3-tuple teruggeeft.
{- | Je krijgt een tuple met 3 waardes mee, je patternmatch op die tuple en dan op het eerste element daarvan. Wat de 2 andere elementen zijn maakt niet uit. Je return het eerste element.
-}
fst3 :: (a, b, c) -> a
fst3 (a, _, _) = a

-- TODO Maak een functie `uncurry3` die een functie met drie argumenten transformeert naar een functie die een 3-tuple als argument neemt.
{- | Je krijgt een functie mee en een tuple met 3 waardes. De functie heeft 3 input waardes nodig. Je returnt gewoon de uitkomst van de functie met als argumenten de 3 waardes uit de tuple
-}
uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a, b, c) = f a b c
