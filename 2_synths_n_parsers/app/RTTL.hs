{-|
    Module      : RTTL
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat een main-functie voor het lezen van user-supplied RTTL ringtones en het genereren van de behorende audio.
-}

module Main (main) where

import Data.Maybe (fromMaybe)
import Types (Instrument)
import Instruments (defaultInstrument , defaultSquare , defaultTriangle , pop , twisted , bass , kick , noise)
import Data (ppk)
import IO (playRTTL)

-- TODO Schrijf een main-functie die de gebruiker om een RTTL encoded String vraagt, het `instrumentMenu` print en vervolgens een getal overeenkomstig met een instrument. De string wordt met het gekozen element met `playRTTL` afgespeeld. Als er geen geldig instrument wordt meegegeven wordt `defaultInstrument` gepakt.
{- | Hij vraagt via de console om een rttl string daarna om een instrument. Dan kijkt die of het gekregen instrument nummer dat die krijgt geldig is dus of die het nummer naar een int kan casten. Zo ja roept die playRTTL aan met het instrument nummer en de rttl string. Zo niet roept die playRTTL aan met defaultInstrument en de rttl string.
-}
main :: IO ()
main = do putStrLn "Geef een RTTL string: " -- ^ putStrLn print een string in de console
          rttl <- getLine -- ^ getLine zet de terug gekregen waarde in variabele rttl
          putStrLn instrumentMenu -- ^ print het instrument menu in de console
          ins <- getLine -- ^ getLine zet de terug gekregen waarde in variabele ins
          case chooseInstrument (read ins) of 
            (Just i ) -> playRTTL i rttl
            Nothing -> playRTTL defaultInstrument rttl

instrumentMenu :: String
instrumentMenu = unlines [ "1: sine"
                         , "2: square"
                         , "3: triangle"
                         , "4: pop"
                         , "5: twisted"
                         , "6: bass"
                         , "7: kick"
                         , "8: noise"
                         ]

-- TODO Schrijf een functie `chooseInstrument` die een `Int` interpreteert tot een `Maybe Instrument` volgens de tabel hierboven.
{- | Hij krijgt een int mee dan kijkt die of die int 1, 2, 3, 4, 5, 6, 7 of 8 is. Zo ja geeft die het bijbehordende instrument terug. Zo niet geeft die nothing terug.
-}
chooseInstrument :: Int -> Maybe Instrument
chooseInstrument i = case i of 1 -> Just defaultInstrument  
                               2 -> Just defaultSquare 
                               3 -> Just defaultTriangle 
                               4 -> Just pop 
                               5 -> Just twisted 
                               6 -> Just bass 
                               7 -> Just kick 
                               8 -> Just noise
                               _ -> Nothing
